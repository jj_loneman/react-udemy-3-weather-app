import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action)  {
  switch (action.type) {
    case FETCH_WEATHER:
      // Returns a new instance of state because we don't want to
      // mutate the existing state
      return [ action.payload.data, ...state ];
  }

  return state;
}